import { Component, OnInit } from '@angular/core';
import { Article } from './models/article';
import { ArticleService } from '../../services/article.service';
import { from } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ArticleService]
})
export class AppComponent {
  public page_title = 'Articulos';
    
}

