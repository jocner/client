import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { ProductDataSource, ProductItem } from './product-datasource';
import { Article } from '../models/article';
import { ArticleService } from '../services/article.service';
import { from } from 'rxjs';
import { error } from 'util';
import Icon from '@material-ui/core/Icon';
import { Table } from '@material-ui/core';

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [ArticleService]
})
export class ProductComponent implements  OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<ProductItem>;
  dataSource: ProductDataSource;

  public name: string;
  public description: string;
  public date: string;
  public action: string;
  public article: Article;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
 // displayedColumns = ['id', 'name', 'description', 'date', 'action'];


  constructor(
    private _articleService : ArticleService
) { 
 this.name = 'Articulo';
 this.description = 'Descripcion';
 this.date = 'Fecha';
 this.action = 'Accion';
 
} 

  ngOnInit() {
    this.dataSource = new ProductDataSource();
    this.getArticle();
    
  }

  getArticle(){
    this._articleService.getArticle().subscribe(
      response => {
       
         if(response.articles){
             this.article = response.articles;
             console.log(response.articles);
             console.log("articles");
         }
      },
      error => {
        console.log(error);
      }

    );
  }

  deleteArticle(id){
    this._articleService.deleteArticle(id).subscribe(
      response => {
        this.getArticle();
        console.log('metodo delete');
       
      },
      error => {
        console.log(error);
        
      }
      
    );
  }
/*
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  } */
}
